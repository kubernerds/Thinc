# iSAS

## Build and run
Run the following command from the root of this repo.
1. `sh ./bin/start`
2. `sh ./bin/minikube-build`

Make shure to use an SQL Server management tool and load the database backup!

## Deploy all Kubernetes configs in this repo
run `sh ./bin/deploy`

## Stop
run `sh ./bin/stop`
