using Microsoft.AspNetCore.Mvc;

namespace Thinc.Controllers
{
    [Route("/")]
    [ApiController]
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Get()
        {
            return new RedirectResult("/Default.aspx?F=Home");
        }
    }
}