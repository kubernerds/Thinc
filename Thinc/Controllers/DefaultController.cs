﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thinc.Services;

namespace Thinc.Controllers
{
    [Route("/Default.aspx")]
    [ApiController]
    public class DefaultController : Controller
    {
        [HttpGet]
        public ContentResult Get()
        {
            var nvQs = HttpContext.Request.Query;
            var sFormName = !string.IsNullOrEmpty(nvQs["F"]) ? (string) nvQs["F"] : "Home";

            var sProcedureName = DataAccess.getDrawProcedure(sFormName, out var bHasAction);

            if (string.IsNullOrEmpty(sProcedureName))
            {
                return new ContentResult
                {
                    ContentType = "text/html",
                    StatusCode = (int) HttpStatusCode.BadRequest
                };
            }

            var response = new StringBuilder();


            //Execute stored procedure
            SqlCommand cmd = DataAccess.SetupStoredProcedure(sProcedureName,
                HttpUtility.ParseQueryString(Request.QueryString.ToString()));
            if (cmd != null)
            {
                try
                {
                    using (var reader = cmd.ExecuteXmlReader())
                    {
                        //Response.Clear();
                        var bIsAjaxRequest = Request.Headers["X-Requested-With"].ToString() != null &&
                                              Request.Headers["X-Requested-With"].ToString().ToLower() ==
                                              "xmlhttprequest";
                        if (!bIsAjaxRequest)
                        {
                            response.Append(GetHead());
                            response.Append("<body>");
                        }

                        var sXml = string.Empty;
                        while (reader.Read())
                        {
                            sXml += reader.ReadOuterXml();
                        }

                        if (sXml.StartsWith("<redirect>") && sXml.EndsWith("</redirect>"))
                        {
                            var sRedirectLocation = sXml.Remove(0, 10).Replace("</redirect>", string.Empty)
                                .Replace("&amp;", "&");
                            return new ContentResult
                            {
                                ContentType = "text/html",
                                StatusCode = (int) HttpStatusCode.OK,
                                Content = sRedirectLocation
                            };
                        }
                        else
                        {
                            response.Append(ProcessXml(sXml, sFormName, Request.QueryString.ToString(), bHasAction));
                            if (!bIsAjaxRequest)
                            {
                                response.Append("</body></html>");
                            }

                            return new ContentResult
                            {
                                ContentType = "text/html",
                                StatusCode = (int) HttpStatusCode.OK,
                                Content = response.ToString()
                            };
                        }
                    }
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return null;
        }

        public ActionResult Post()
        {
            
            var response = new StringBuilder();
            var nvQs = new NameValueCollection();
            foreach (var (key, value) in Request.Form)
            {
                nvQs.Set(key, value);
            }

            var sProcedureName = string.Empty;
            var bHasAction = false;

            var nvOldQs = HttpUtility.ParseQueryString(nvQs["ThinC_QS"]);
            var sFormName = nvQs["ThinC_F"];
            if (nvOldQs.Count > 0)
            {
                nvQs = Merge(nvOldQs, nvQs);
            }

            var sActionProcName = DataAccess.getActionProcedure(sFormName);
            if (string.IsNullOrEmpty(sActionProcName))
            {
                return new RedirectResult("Default.aspx?F=Home", false);
            }

            if (Request.Form.Files.Count > 0)
            {
                Handle_FileUploads(nvQs, sFormName);
            }

            var cmdPost = DataAccess.SetupStoredProcedure(sActionProcName, nvQs,
                "Nr of files: " + Request.Form.Files.Count);
            if (cmdPost != null)
            {
                object objReturn;
                try
                {
                    objReturn = cmdPost.ExecuteScalar();
                }
                finally
                {
                    cmdPost.Connection.Close();
                }

                if (objReturn != null)
                {
                    var bIsAjaxRequest = Request.Headers["X-Requested-With"].ToString() != null &&
                                          Request.Headers["X-Requested-With"].ToString().ToLower() ==
                                          "xmlhttprequest";

                    if (bIsAjaxRequest)
                    {
                        response.Append(objReturn);
                    }
                    else
                    {
                        return new RedirectResult(objReturn.ToString(), false);
                    }
                }

                sProcedureName = DataAccess.getDrawProcedure("Home", out bHasAction);
            }

            if (string.IsNullOrEmpty(sProcedureName))
            {
                return new ContentResult
                {
                    Content = response.ToString()
                };
            }


            //Execute stored procedure
            var cmd = DataAccess.SetupStoredProcedure(sProcedureName,
                HttpUtility.ParseQueryString(Request.QueryString.ToString()));
            if (cmd != null)
            {
                try
                {
                    using (var reader = cmd.ExecuteXmlReader())
                    {
                        //Response.Clear();
                        var bIsAjaxRequest = Request.Headers["X-Requested-With"].ToString() != null &&
                                             Request.Headers["X-Requested-With"].ToString().ToLower() ==
                                             "xmlhttprequest";
                        if (!bIsAjaxRequest)
                        {
                            response.Append(GetHead());
                            response.Append("<body>");
                        }

                        var sXml = string.Empty;
                        while (reader.Read())
                        {
                            sXml += reader.ReadOuterXml();
                        }

                        if (sXml.StartsWith("<redirect>") && sXml.EndsWith("</redirect>"))
                        {
                            var sRedirectLocation = sXml.Remove(0, 10).Replace("</redirect>", string.Empty)
                                .Replace("&amp;", "&");
                            return new RedirectResult(sRedirectLocation, false);
                        }
                        else
                        {
                            response.Append(ProcessXml(sXml, sFormName, Request.QueryString.ToString(), bHasAction));
                            if (!bIsAjaxRequest)
                            {
                                response.Append("</body></html>");
                            }

                            return new ContentResult
                            {
                                ContentType = "text/html",
                                StatusCode = (int) HttpStatusCode.OK,
                                Content = response.ToString()
                            };
                        }
                    }
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
            
            return new ContentResult
            {
                Content = response.ToString()
            };
        }

        private async void Handle_FileUploads(NameValueCollection nvQs, string sFormName)
        {
            var sUploadDir = DataAccess.getAbsoluteUploadDirectory(sFormName, "recch", out var isArchive);
            const string sUploadProcedure = "spMobile_AddDocument";

            foreach (var file in Request.Form.Files)
            {
                if (!string.IsNullOrEmpty(file.FileName))
                {
                    if (!string.IsNullOrEmpty(sUploadDir) && !string.IsNullOrEmpty(sUploadProcedure))
                    {
                        var sSanitizedFileName =
                            DataAccess.CreateValidWindowsFileName(file.FileName);
                        var sFileNameonDisk = DataAccess.GetUniqueFilename(sUploadDir, sSanitizedFileName);
                        try
                        {
                            //Bestaat directory wel? 
                            if (!Directory.Exists(sUploadDir + Path.DirectorySeparatorChar))
                            {
                                //Aanmaken!
                                Directory.CreateDirectory(sUploadDir + Path.DirectorySeparatorChar);
                            }

                            SaveFiles(file, sUploadDir + Path.DirectorySeparatorChar + sFileNameonDisk);
                            var nvcDoc = new NameValueCollection();
                            nvcDoc.Add("uds_OriginalName", sSanitizedFileName);
                            nvcDoc.Add("uds_DocumentName", sFileNameonDisk);
                            nvcDoc.Add("uds_AdditionalDirectory",
                                (isArchive ? DataAccess.CreatePathNameFromUsername("recch") : null));
                            nvcDoc.Add("uds_ByteSize", file.Length.ToString());

                            var cmdFile = DataAccess.SetupStoredProcedure(sUploadProcedure, nvcDoc);
                            if (cmdFile != null)
                            {
                                var oRet = cmdFile.ExecuteScalar();
                                cmdFile.Connection.Close();
                                if ((oRet != null) && (int.TryParse(oRet.ToString(), out _)))
                                {
                                    //find which field had this fileupload
                                    nvQs.Add(file.Name, oRet.ToString());
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            nvQs.Add(file.Name, null);
                        }
                    }
                }
            }
        }

        private void SaveFiles(IFormFile file, string filePathName)
        {
            using (var stream = new FileStream(filePathName, FileMode.Create))
            {
                file.CopyTo(stream);
            }
        }

        private static NameValueCollection Merge(NameValueCollection first, NameValueCollection second)
        {
            if (second == null)
            {
                return first;
            }

            foreach (string item in second)
            {
                if (((IList) first.AllKeys).Contains(item))
                {
                    // if first already contains this item, update it to the value of second
                    first[item] = second[item];
                }
                else
                {
                    // otherwise add it
                    first.Add(item, second[item]);
                }
            }

            return first;
        }


        private string GetHead()
        {
            var rFs = new FileStream($"{Directory.GetCurrentDirectory()}/wwwroot/ThinC.html", FileMode.Open,
                FileAccess.Read);
            var reader = new StreamReader(rFs);
            var sHeadTemplate = reader.ReadToEnd();
            reader.Close();
            rFs.Close();

            sHeadTemplate = sHeadTemplate.Replace("{0}", "iSAS");
            return sHeadTemplate;
        }

        private static string ProcessXml(string sXml, string sFormName, string sQueryString, bool bHasAction)
        {
            if ((bHasAction) && ((sXml.Contains("<input") || (sXml.Contains("<select")) || (sXml.Contains("<button")) ||
                                  (sXml.Contains("<textarea")))))
            {
                //form elements found
                string sHiddenField = "<input type=\"hidden\" name=\"ThinC_F\" value=\"" +
                                      HttpUtility.HtmlAttributeEncode(sFormName) + "\" />";
                sHiddenField += "<input type=\"hidden\" name=\"ThinC_QS\" value=\"" +
                                HttpUtility.HtmlAttributeEncode(sQueryString) + "\" />";

                //does a form exist?
                int iIndex = sXml.IndexOf("<form");
                if (iIndex >= 0)
                {
                    //yes; only write hidden field
                    iIndex = sXml.IndexOf('>', iIndex + 1);
                    sXml = sXml.Substring(0, iIndex + 1) + sHiddenField + sXml.Substring(iIndex + 1);
                }
                else
                {
                    //no, assume first div is page
                    iIndex = sXml.IndexOf('>', sXml.IndexOf("<div") + 1);
                    int iIndex2 = sXml.LastIndexOf('<');
                    sXml = sXml.Substring(0, iIndex + 1) + "<form method=\"post\" data-ajax=\"false\">" + sHiddenField +
                           sXml.Substring(iIndex + 1, iIndex2 - iIndex - 1) + "</form></div>";
                }
            }

            return sXml;
        }
    }
}