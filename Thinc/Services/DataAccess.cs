using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace Thinc.Services
{
    static class DataAccess
    {
        internal static string CreatePathNameFromUsername(string sUsername)
        {
            foreach (char c in Path.GetInvalidPathChars())
            {
                if (sUsername.Contains(c))
                {
                    sUsername = sUsername.Replace(c, '_');
                }
            }

            return sUsername;
        }

        internal static string CreateValidWindowsFileName(string sFileName)
        {
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                if (sFileName.Contains(c))
                {
                    sFileName = sFileName.Replace(c, '_');
                }
            }

            return sFileName;
        }

        internal static string GetUniqueFilename(string sPath, string fileName)
        {
            bool bOK = false;
            string sResultFile = fileName;
            do
            {
                // encode a datetime to make the filename unique
                string dateStr = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-ffffff");
                string extension = Path.GetExtension(sResultFile);

                // remove all non-letters and -digits from the fileName
                // shorten fileName to ensure a maximum path length
                sResultFile = sResultFile.Replace('.', '_').Trim();
                StringBuilder sb = new StringBuilder(sResultFile);
                int i = 0;
                while (i < sb.Length)
                {
                    if ((Char.IsLetterOrDigit(sb[i])) || (sb[i] == '_'))
                    {
                        i++;
                    }
                    else
                    {
                        sb.Remove(i, 1);
                    }
                }

                sResultFile = sb.ToString();
                if (String.IsNullOrEmpty(fileName))
                {
                    fileName = "noname";
                }

                // add an extension which is incremented until a unique filename is formed
                i = 0;
                sResultFile = String.Format("{0}_{1}{2}", sResultFile, dateStr, extension);
                bOK = !(File.Exists(sPath + sResultFile));
            } while (!bOK);

            return sResultFile;
        }

        internal static string getActionProcedure(string sFormName)
        {
            DataRow dr = getMetaDataRow(sFormName);
            if (dr == null) return null;
            return dr["ActionProcedure"].ToString();
        }

        internal static bool isArchive(string sFormName)
        {
            DataRow dr = getMetaDataRow(sFormName);
            if (dr == null) return false;
            return (bool) dr["UploadToArchive"];
        }

        internal static string AdditionalDirectory(string sFormName)
        {
            DataRow dr = getMetaDataRow(sFormName);
            if (dr == null) return null;
            return dr["RelativeUploadPath"].ToString();
        }

        internal static string getAbsoluteUploadDirectory(string sFormName, string sUsername, out bool isArchive)
        {
            DataRow dr = getMetaDataRow(sFormName);
            isArchive = false;
            if (dr == null) return null;

            string sAdditionalDirectory = dr["RelativeUploadPath"].ToString();

            string sDir;
            if ((bool) dr["UploadToArchive"])
            {
                sDir = "/mnt/uploads/archive";
                if (!String.IsNullOrEmpty(sAdditionalDirectory))
                {
                    sDir += Path.DirectorySeparatorChar + sAdditionalDirectory;
                }

                sDir += Path.DirectorySeparatorChar + CreatePathNameFromUsername(sUsername);
                isArchive = true;
            }
            else
            {
                sDir = "/mnt/uploads/documents";

                if (!String.IsNullOrEmpty(sAdditionalDirectory))
                {
                    sDir += Path.DirectorySeparatorChar + sAdditionalDirectory;
                }
            }

            return sDir;
        }

        internal static string getDrawProcedure(string sFormName, out bool HasAction)
        {
            HasAction = false;
            DataRow dr = getMetaDataRow(sFormName);
            if (dr == null) return null;
            HasAction = (!string.IsNullOrEmpty(dr["ActionProcedure"].ToString()));
            return dr["DrawProcedure"].ToString();
        }

        private static DataRow getMetaDataRow(string sFormName)
        {
            DataTable dtResult = getMetaData().FilterDataTable("Pagename = '" + sFormName + "'", null);
            if (dtResult.Rows.Count > 0)
            {
                return dtResult.Rows[0];
            }

            return null;
        }

        private static DataTable getMetaData()
        {
            var sProcedurename = "spUFGMobile_GetPages";
            var cmdMeta = SetupStoredProcedure(sProcedurename, null);
            var daMeta = new SqlDataAdapter(cmdMeta);
            var oCache = new DataTable();
            daMeta.Fill(oCache);
            cmdMeta.Connection.Close();
            return oCache;
        }

        /// <summary>returns the datatable with a filtered and sorted resultset..</summary>
        /// <param name="dtOriginal">Datatable to be filtered</param>
        /// <param name="sFilter">SQL like Where clause to filter the datatable e.g. columnname='columnvalue'</param>
        /// <param name="sSort">SQL Like Order By clause e.g. columnname1 DESC, Columnname2 ASC</param>
        /// <returns>Datatable filtered by sFilter, ordered by sSort</returns>
        public static DataTable FilterDataTable(this DataTable dtOriginal, string sFilter, string sSort)
        {
            return new DataView(dtOriginal, sFilter, sSort, DataViewRowState.CurrentRows).ToTable();
        }

        private static bool getParamsOfProcedure(SqlCommand cmd)
        {
            if (cmd.CommandType != CommandType.StoredProcedure) return false;
            cmd.Connection.Open();
            try
            {
                SqlCommandBuilder.DeriveParameters(cmd);
            }
            catch (Exception e)
            {
                LogError(cmd.Connection,
                    "Failed to derive paramaters of: " + cmd.CommandText + " because of: " + e.Message);
                return false;
            }

            return true;
        }

        internal static SqlCommand SetupStoredProcedure(string sProcedureName, NameValueCollection nvParams,
            string sAdditionalLogMessage = "")
        {
            if (String.IsNullOrEmpty(sProcedureName))
            {
                LogError("ProcedureName is empty.. aborting");
                return null;
            }

            var connectionString = $"Data Source={Environment.GetEnvironmentVariable("DB_HOST")};" +
                                   $"Initial Catalog={Environment.GetEnvironmentVariable("DB_DATABASE")};" +
                                   $"User id={Environment.GetEnvironmentVariable("DB_USER")};" +
                                   $"Password={Environment.GetEnvironmentVariable("DB_PASSWORD")};";

            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = sProcedureName;

            if (getParamsOfProcedure(cmd))
            {
                foreach (SqlParameter prm in cmd.Parameters)
                {
                    string sParamName = prm.ParameterName.Substring(1);
                    switch (sParamName)
                    {
                        case "UFGUsername":
                            prm.Value = "recch";
                            break;
                        case "UFGHostname":
                            prm.Value = "127.0.0.1";
                            break;
                        case "UFGBrowser":
                            prm.Value =
                                "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0";
                            break;
                        case "UFGBrowserName":
                            prm.Value = "Firefox";
                            break;
                        case "UFGBrowserVersion":
                            prm.Value = "66";
                            break;
                        case "UFGWebserverHostName":
                            prm.Value = "Kubernetes";
                            break;
                        case "UFGFormRequest":
                            prm.TypeName = "ThinC_MultiValue";
                            DataTable dtParams = new DataTable();
                            dtParams.Columns.Add("ParamName", typeof(string));
                            dtParams.Columns.Add("ColumnValue", typeof(string));
                            for (int i = 0; i < nvParams.Count; i++)
                            {
                                string sKey = nvParams.Keys[i];
                                if ((sKey != "__VIEWSTATE") && (sKey != "ThinCRef"))
                                {
                                    DataRow drNew = dtParams.NewRow();
                                    drNew[0] = sKey;
                                    drNew[1] = nvParams[i];
                                    dtParams.Rows.Add(drNew);
                                    dtParams.AcceptChanges();
                                }
                            }

                            prm.Value = dtParams;
                            break;
                        default:
                            //no default param
                            if (prm.Direction == ParameterDirection.Input ||
                                prm.Direction == ParameterDirection.InputOutput)
                            {
                                if (nvParams.AllKeys.Contains(sParamName))
                                {
                                    switch (prm.SqlDbType)
                                    {
                                        case SqlDbType.TinyInt:
                                            int iValue;
                                            if (int.TryParse(nvParams[sParamName], out iValue) && iValue >= 0 &&
                                                iValue <= 255)
                                            {
                                                prm.Value = iValue;
                                            }
                                            else
                                            {
                                                LogError(connection,
                                                    "Value: " + nvParams[sParamName] + " of parameter:" + sParamName +
                                                    " of procedure " + sProcedureName + " cannot be parsed to Int16");
                                                return null;
                                            }

                                            break;


                                        case SqlDbType.Int:
                                        case SqlDbType.SmallInt:
                                            int iValue32;
                                            if (int.TryParse(nvParams[sParamName], out iValue32))
                                            {
                                                prm.Value = iValue32;
                                            }
                                            else
                                            {
                                                LogError(connection,
                                                    "Value: " + nvParams[sParamName] + " of parameter:" + sParamName +
                                                    " of procedure " + sProcedureName +
                                                    " cannot be parsed in to Int32");
                                                return null;
                                            }

                                            break;

                                        case SqlDbType.BigInt:
                                            long iValue64;
                                            if (long.TryParse(nvParams[sParamName], out iValue64))
                                            {
                                                prm.Value = iValue64;
                                            }
                                            else
                                            {
                                                LogError(connection,
                                                    "Value: " + nvParams[sParamName] + " of parameter:" + sParamName +
                                                    " of procedure " + sProcedureName + " cannot be parsed to Int64");
                                                return null;
                                            }

                                            break;

                                        case SqlDbType.Date:
                                            try
                                            {
                                                DateTime dt = DateTime.ParseExact(nvParams[sParamName], "yyyy-MM-dd",
                                                    null);
                                                //2010-09-01T00:00:00
                                                prm.Value = dt;
                                            }
                                            catch
                                            {
                                                LogError(connection,
                                                    "Value: " + nvParams[sParamName] + " of parameter:" + sParamName +
                                                    " of procedure " + sProcedureName +
                                                    " cannot be parsed in format [yyy-MM-dd]");
                                                return null;
                                            }

                                            break;
                                        case SqlDbType.DateTime:
                                        case SqlDbType.DateTime2:
                                            try
                                            {
                                                DateTime dt = DateTime.ParseExact(nvParams[sParamName],
                                                    "yyyy-MM-ddTHH:mm:ss.fff", null);
                                                //2010-09-01T00:00:00.000
                                                prm.Value = dt;
                                            }
                                            catch
                                            {
                                                try
                                                {
                                                    DateTime dt = DateTime.ParseExact(nvParams[sParamName],
                                                        "yyyy-MM-ddTHH:mm:ss", null);
                                                    //2010-09-01T00:00:00
                                                    prm.Value = dt;
                                                }
                                                catch
                                                {
                                                    //not a valid datetime.. crash!
                                                    LogError(connection,
                                                        "Value: " + nvParams[sParamName] + " of parameter:" +
                                                        sParamName + " of procedure " + sProcedureName +
                                                        " cannot be parsed in format [yyyy-MM-ddTHH:mm:ss]");
                                                    return null;
                                                }
                                            }

                                            break;
                                        default:
                                            prm.Value = nvParams[sParamName].Trim();
                                            break;
                                    }
                                }
                                else
                                {
                                    LogError(connection,
                                        "Parameter: " + sParamName + " of procedure " + sProcedureName +
                                        " is not given a value, aborting! - Additional: " + sAdditionalLogMessage,
                                        (sParamName.ToLower() != "uploaddoc"));
                                    return null;
                                }
                            }

                            break;
                    }
                }

                return cmd;
            }

            return null;
        }

        private static void LogError(string s)
        {
            Console.WriteLine(s);
        }

        private static void LogError(SqlConnection connection, string s)
        {
            Console.WriteLine(s);
        }

        private static void LogError(SqlConnection connection, string s, bool b)
        {
            Console.WriteLine(s);
        }
    }
}